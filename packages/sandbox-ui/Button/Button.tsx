import React, { FC, ReactNode } from 'react'

type TButtonProps = {
  children?: ReactNode
}

const Button: FC<TButtonProps> = ({ children }) => {
  return <button>{children}</button>
}

export default Button
